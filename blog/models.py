from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse


class Tags(models.Model):
    name = models.CharField(max_length=20, db_index=True, verbose_name='Название')

    def get_absolute_url(self):
        return reverse('tags', kwargs={'tags': self.id})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Тэги"
        verbose_name = "Тэг"
        ordering = ["name"]


class Categories(models.Model):
    name = models.CharField(max_length=35, db_index=True, verbose_name='Категория')

    def get_absolute_url(self):
        return reverse('category', kwargs={'category': self.id})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Категории"
        verbose_name = "Категория"


class Subcategories(models.Model):
    name = models.CharField(max_length=35, db_index=True, verbose_name='Подкатегория')
    category = models.ForeignKey(Categories, on_delete=models.PROTECT)

    def get_absolute_url(self):
        return reverse('subcategory', kwargs={'subcategory': self.id})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Подкатегория"
        verbose_name_plural = "Подкатегории"


class PostModel(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')
    title = models.CharField("Заголовок поста", max_length=50)
    picture = models.ImageField("Картинка", upload_to="images", blank=True, null=True)
    text = models.TextField("текст")
    comment = models.IntegerField("Счетчик комментариев", default=0)
    browsing = models.IntegerField("Количество просмотров", default=0)
    like = models.PositiveIntegerField("Счетчик лайков", default=0)
    data = models.DateField(auto_now=True)
    tags = models.ManyToManyField(Tags, verbose_name="Тэг", blank=True)
    category = models.ForeignKey(Subcategories, verbose_name='Подкатегория', blank=True, null=True, on_delete=models.PROTECT)

    def __str__(self):
        return self.title

    def get_author(self):
        author = User.objects.get(posts=self)
        return author

    def get_absolute_url(self):
        return reverse("detail", kwargs={'pk': self.id})

    class Meta:
        verbose_name = "Статью"
        verbose_name_plural = "Статьи"
        ordering = ["-id"]


class Comments(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE, related_name='user')
    post_model = models.ForeignKey(PostModel, on_delete=models.CASCADE, related_name='comments_post', blank=True, null=True)
    text = models.TextField("Текст с комментарием", max_length=200)
    data = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.post_model.author.username

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"


class Likes(models.Model):
    post = models.ForeignKey(PostModel, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.post.title

    class Meta:
        verbose_name = "Лайк"
        verbose_name_plural = "Лайки"
        unique_together = ['post', 'user']