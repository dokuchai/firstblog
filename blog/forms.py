from django import forms
from blog.models import PostModel, Comments
from account.models import Profile


class PostForm(forms.ModelForm):
    class Meta:
        model = PostModel
        fields = ["picture", "title", "text", 'category', 'tags']


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comments
        fields = ["text"]


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['user_birth', 'user_city', 'user_bio']


