from django.shortcuts import render
from django.views.generic import DetailView
from rest_framework import generics

from account.forms import UserRegistrationForm
from blog.serializers import ProfileSerializer
from .models import Profile
from django.contrib.auth.views import LoginView, LogoutView


def register(request):
    if request.method == "POST":
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            return render(request, 'register_done.html', {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request, 'register.html', {'user_form': user_form})


class ProfileView(DetailView):
    model = Profile
    template_name = 'profile.html'
    context_object_name = 'profiles'


class Login(LoginView):
    redirect_field_name = 'index.html'


class Logout(LogoutView):
    template_name = 'logout.html'


class APIProfileEdit(generics.RetrieveUpdateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def get_object(self):
        obj = Profile.objects.get(user=self.request.user)
        return obj
