from django.urls import path
from .views import ProfileView
from account.views import register, Login, Logout


urlpatterns = [
    path('account/<int:pk>', ProfileView.as_view(), name='profile'),
    path('register/', register, name='register'),
    path('login/', Login.as_view(), name='login'),
    path('logout/', Logout.as_view(), name='logout'),
]
