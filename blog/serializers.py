from rest_framework import serializers
from .models import PostModel, Comments
from account.models import Profile


class ProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = '__all__'


class CommentsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comments
        fields = ('user', 'text', 'post_model',)


class PostSerializer(serializers.ModelSerializer):
    author = serializers.CharField(source='get_author', read_only=True)
    author_id = serializers.IntegerField()
    comments_post = CommentsSerializer(many=True, read_only=True)

    class Meta:
        model = PostModel
        fields = ('id', 'title', 'author', 'author_id', 'text', 'comments_post')


