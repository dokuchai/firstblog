from django.shortcuts import render, redirect
from blog.models import *
from blog.forms import PostForm, CommentForm, ProfileForm
from blog.serializers import PostSerializer, CommentsSerializer
from account.models import Profile
from django.views.generic import ListView
from django.db.models import Q
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from blog.services.logic import *


def index(request):
    if request.method == "GET":
        blogs = PostModel.objects.all()[0:10]
        categories = Categories.objects.all()
        subcategories = Subcategories.objects.all()
        popular = PostModel.objects.all().order_by('-browsing')[0:5]
        context = {
            "blogs": blogs,
            'populars': popular,
            'categories': categories,
            'subcategories': subcategories,
        }
        return render(request, "index.html", context)


def detailPost(request, pk):
    if request.user.is_authenticated:
        if request.method == "GET":
            detail1 = PostModel.objects.get(id=pk)
            detail1.browsing += 1
            detail1.save()
            rating = Rating(like=detail1.like, browsing=detail1.browsing).total()
            tags = detail1.tags.all()
            form = CommentForm
            comments = Comments.objects.filter(post_model_id=pk).order_by("-id")
            context = {
                "detail1": detail1,
                "form": form,
                "comments": comments,
                "tags": tags,
                'rating': rating
            }
            return render(request, "single.html", context)
    else:
        return redirect('index')


def post_new(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = PostForm(request.POST, request.FILES)
            if form.is_valid():
                post = PostModel.objects.create(author=request.user, title=form.cleaned_data['title'],
                                                picture=form.cleaned_data['picture'],
                                                text=form.cleaned_data['text'],
                                                category=form.cleaned_data['category'],
                                                )
                post.save()
                post.tags.add(request.POST.get('tags'))
                post.save()
                return redirect('index')
            else:
                return redirect('post_new')
        else:
            form = PostForm()
            return render(request, 'post_edit.html', {'form': form})


def comments(request, pk):
    if request.method == "POST":
        cs = PostModel.objects.get(id=pk)
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = Comments.objects.create(text=form.cleaned_data['text'], post_model_id=pk, user=request.user)
            comment.save()
            cs.comment += 1
            cs.save()
            return redirect("detail", pk=pk)
        else:
            return redirect("index")


def likes(request, pk, pk2):
    if request.method == "GET":
        try:
            likes = Likes.objects.create(post_id=pk, user_id=pk2)
            likes.save()
            post = PostModel.objects.get(id=pk)
            post.like += 1
            post.save()
            return redirect("detail", pk=pk)
        except:
            return redirect("detail", pk=pk)


def tags(request, pk):
    if request.method == "GET":
        post = Tags.objects.get(id=pk).postmodel_set.all()
        context = {'posts': post}
        return render(request, 'list.html', context)


def categories(request, pk):
    if request.method == "GET":
        subcategory = Categories.objects.get(id=pk).subcategories_set.all()
        context = {'subcategories': subcategory}
        return render(request, 'categories.html', context)


def subcategories(request, pk):
    if request.method == "GET":
        post = Subcategories.objects.get(id=pk).postmodel_set.all()
        context = {'posts': post}
        return render(request, 'subcategories.html', context)


def profile_edit(request):
    if request.method == "POST":
        profiles = Profile.objects.get(user_id=request.user.id)
        form = ProfileForm(request.POST, instance=profiles)
        if form.is_valid():
            form.save()
            return redirect('profile', pk=request.user.id)
        else:
            return redirect('index')
    else:
        form = ProfileForm(request.POST)
        return render(request, 'profile_edit.html', {'form': form})


class SearchResultsView(ListView):
    model = PostModel
    template_name = 'search_list.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        queryset = PostModel.objects.filter(Q(title__icontains=query) | Q(text__icontains=query))
        return queryset


class APIPosts(generics.ListCreateAPIView):
    queryset = PostModel.objects.all()
    serializer_class = PostSerializer


class APIPostDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = PostModel.objects.all()
    serializer_class = PostSerializer


class APINewComment(generics.ListCreateAPIView):
    queryset = Comments.objects.all()
    serializer_class = CommentsSerializer
