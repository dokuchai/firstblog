class Rating:

    def __init__(self, like, browsing):
        self.like = like
        self.browsing = browsing

    def total(self):
        return self.like * self.browsing
