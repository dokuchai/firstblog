from django.db import models
from django.contrib.auth.models import User
from blog.models import PostModel, Comments
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_city = models.CharField('Город', max_length=300, blank=True)
    user_birth = models.CharField('дата рождения', blank=True, null=True, max_length=10)
    user_karma = models.IntegerField('Карма пользователя', default=0)
    user_like = models.IntegerField('Оцененные статьи', default=0)
    user_bio = models.TextField('Напишите что-нибудь о себе', max_length=1000, blank=True)
    user_best = models.IntegerField('Избранные статьи', default=0)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = 'Профили'
        verbose_name = 'Профиль'

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance, id=instance.id)
