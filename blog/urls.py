from django.urls import path

from account.views import APIProfileEdit
from blog.views import *

urlpatterns = [
    path('', index, name="index"),
    path("detail/<int:pk>/", detailPost, name="detail"),
    path("post-new/", post_new, name="post_new"),
    path('add-comment/<int:pk>/', comments, name="add_comment"),
    path('likes/<int:pk>/<int:pk2>/', likes, name="likes"),
    path('tags/<int:pk>/', tags, name='tags'),
    path('profile-edit/', profile_edit, name='profile_edit'),
    path('search/', SearchResultsView.as_view(), name='search_results'),
    path('category/<int:pk>/', categories, name='category'),
    path('subcategories/<int:pk>/', subcategories, name='subcategory'),
    path('api/posts/', APIPosts.as_view()),
    path('api/posts/<int:pk>/', APIPostDetail.as_view()),
    path('api/comment/', APINewComment.as_view()),
    path('api/profile/', APIProfileEdit.as_view()),
]
