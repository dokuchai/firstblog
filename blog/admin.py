from django.contrib import admin
from blog.models import PostModel, Likes, Tags, Comments, Categories, Subcategories


@admin.register(PostModel)
class BlogAdmin(admin.ModelAdmin):
    list_display = ("title",)


@admin.register(Likes)
class LikesAdmin(admin.ModelAdmin):
    list_display = ("post",)


@admin.register(Tags)
class TagsAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Comments)
class Comments(admin.ModelAdmin):
    list_display = ('user',)


@admin.register(Categories)
class CategoriesAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Subcategories)
class SubcategoriesAdmin(admin.ModelAdmin):
    list_display = ("name",)
